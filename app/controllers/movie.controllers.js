let Movie = require('../models/movie.model.js');
let _this = this;

//retrieve all movies
exports.index = function(req, res) {
	Movie.find( (err, movie) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'Some error occured while retrieving movies.'});
		} else {
			res.send(movie);
		}
	});
};

//show single
exports.show = function(req, res, _findById) {
	let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send(movie);
	});
};

//create
exports.create = function(req, res) {
	//create and save new movie
	//console.log(req.body);
	if(!req.body.title) {
		return res.status(400).send({message: 'Movie cannot be empty'});
	}
	
	var movie = new Movie({title: req.body.title || "Untitled Movie"});
	
	movie.save( (err, data) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'some error occured white creating the movie'});
		} else {
			res.send(data);
		}
	});
}

//update
exports.update = function(req, res) {
	let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		movie.title = req.body.title;
		
		movie.save((err, data) => {
			if(err) {
				res.status(500).send({message: 'Could not update movie with id ' + _movie_id});
			} else {
				res.send(data);
			}
		});
		
	});
}

//delete
exports.delete = function(req, res) {
	let _movie_id = req.params.movieId;
	
	Movie.findByIdAndRemove(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
            if(err.kind === 'ObjectId') {
                return res.status(404).send({message: "Movie not found with id " + _movie_id});                
            }
            return res.status(500).send({message: "Could not delete movie with id " + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send({message: "Movie deleted successfully!"})
		
	});
}