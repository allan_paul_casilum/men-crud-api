let mongoose = require('mongoose');

let MovieSchema = mongoose.Schema({
	title: String
}, {
	timestamps: true
});

module.exports = mongoose.model('Movie', MovieSchema);