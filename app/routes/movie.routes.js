module.exports = function(app) {
	var movies = require('../controllers/movie.controllers.js');
	
	//index , retrieve all movies
	app.get('/movies', movies.index);
	//create
	app.post('/movie', movies.create);
	//show single movie
	app.get('/movie/:movieId', movies.show);
	//update
	app.put('/movie/:movieId', movies.update);
	//delete
	app.delete('/movie/:movieId', movies.delete);
}