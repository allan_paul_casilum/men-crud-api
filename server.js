const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//create express app
let app = express();

//db
let dbConfig = require('./config/database.config.js');
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.url);
mongoose.connection.on('error', () => {
	console.log('Could not connect to database. Exiting now.');
	process.exit();
});
mongoose.connection.once('open', () => {
	console.log('Successfully connected to the database');
});

//parser request of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//parse request of content-type - application/json
app.use(bodyParser.json());

//define a simple route
app.get('/', (req, res) => {
	res.json({'message':'hello world'});
});

//Require movie routes
require('./app/routes/movie.routes.js')(app);

//listen for request
app.listen(3000, () => {
	console.log('server is listening to 3000');
});